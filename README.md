Horse Grazing Freely
====================

A landscape in a peaceful countryside with a horse grazing freely.

* Create a directory **fonts** inside the directory **$HOME/.local/share**
and add the archive (**poppl-laudatio-medium-condensed.ttf**).

* This will correct the font of the Inkscape SVG file.

**License**
-----------

> This art was created in SVG format Inkscape: you can redistribute
> it and/or modify it under the terms of the GNU General Public License
> as published by the Free Software Foundation, either version 3
> of the License, or (at your option) any later version.

- Copyright (c) 2020 **Carlos Donizete Froes [a.k.a coringao]**
